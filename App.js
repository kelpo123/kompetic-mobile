import React from 'react';
import AppContainer from '@navigations/router';
import {Provider} from 'react-redux';
import {store} from '@redux/store';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import {mapping, light as lightTheme} from '@eva-design/eva';
import {default as appTheme} from './custom-theme.json';
import { FeatherIconsPack } from '@components/ui/icons/feather-icons';
import { AssetIconsPack } from '@components/ui/icons/asset-icons';
import {strings} from 'locales/index';
import firebase from 'react-native-firebase';

const theme = {...lightTheme, ...appTheme};
export default class App extends React.Component {
  componentDidMount() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user){
        this.setState({signedIn: true, checkedSignIn: true});
      }
      else{
        this.setState({signedIn: false, checkedSignIn: true});
      }
    });
  }
  render() {
    return (
      <Provider store={store}>
        <IconRegistry icons={[FeatherIconsPack, AssetIconsPack]} />
        <ApplicationProvider
          mapping={mapping}
          theme={theme}>
          <AppContainer screenProps={{
              strings: strings
            }}/>
        </ApplicationProvider>
      </Provider>
    );
  }
}
