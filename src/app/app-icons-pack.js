import React from 'react';
import {Image, ImageRequireSource} from 'react-native';

/**
 * https://akveo.github.io/react-native-ui-kitten/docs/guides/icon-packages#3rd-party-icon-packages
 */
const IconProvider = (source: ImageRequireSource) => ({
  toReactElement: ({animation, ...style}) => (
    <Image style={style} source={source} />
  ),
});

export const AppIconsPack = {
  name: 'app',
  icons: {
    home: IconProvider(require('@assets/images/icons/home.png')),
    'home-run': IconProvider(require('@assets/images/icons/home-run.png')),
    notif: IconProvider(require('@assets/images/icons/notification.png')),
    'notif-run': IconProvider(
      require('@assets/images/icons/notification-run.png'),
    ),
    profile: IconProvider(require('@assets/images/icons/profile.png')),
    'profile-run': IconProvider(
      require('@assets/images/icons/profile-run.png'),
    ),
    iconic: IconProvider(require('@assets/images/icons/confused.png')),
    'flag-id': IconProvider(require('@assets/images/icons/indonesia.png')),
    sword: IconProvider(require('@assets/images/icons/sword.png')),
    goal: IconProvider(require('@assets/images/icons/goal.png')),
    team: IconProvider(require('@assets/images/icons/team.png')),
    leaderboard: IconProvider(require('@assets/images/icons/trophy.png')),
    archievment: IconProvider(require('@assets/images/icons/medal.png')),
    // dummy
    design: IconProvider(require('@assets/images/dummy/design.png')),
    'dota-2': IconProvider(require('@assets/images/dummy/dota-2.png')),
    gamepad: IconProvider(require('@assets/images/dummy/gamepad.png')),
    logo: IconProvider(require('@assets/images/dummy/logo.png')),
  },
};
