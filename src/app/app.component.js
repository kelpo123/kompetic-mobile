/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {Provider} from 'react-redux';
import {store, persistor} from '@redux/store';
import {PersistGate} from 'redux-persist/integration/react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {FeatherIconsPack} from '@assets/icons/feather-icons';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {AppearanceProvider} from 'react-native-appearance';
import {AppNavigator} from '@navigations/app.navigator';
import {mapping, light as lightTheme} from '@eva-design/eva';
import {default as appTheme} from './app-theme.json';
const theme = {...lightTheme, ...appTheme};
import {strings} from 'locales/index';

const App = () => {
  return (
    <>
      <IconRegistry icons={[EvaIconsPack, FeatherIconsPack]} />
      <AppearanceProvider>
        <Provider store={store}>
          <PersistGate persistor={persistor}>
            <ApplicationProvider mapping={mapping} theme={theme}>
              <SafeAreaProvider>
                <AppNavigator strings={strings} />
              </SafeAreaProvider>
            </ApplicationProvider>
          </PersistGate>
        </Provider>
      </AppearanceProvider>
    </>
  );
};

export default App;
