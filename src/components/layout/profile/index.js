import React from 'react';
import { ThemedLayUserInfo } from './userinfo';

export const LayUserInfo = (props) => (
  <ThemedLayUserInfo {...props} />
);
