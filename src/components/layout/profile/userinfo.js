import React from 'react';
import {Layout} from '@ui-kitten/components';
import {UITitle, UISubtitle} from '@components/ui/typography';
import {UIAvatar} from '@components/ui/avatar';
import {UIIcon, UIIconFeather} from '@components/ui/icons';
import {UIButton} from '@components/ui/button';
import styles from '@assets/styles';
import {View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {isEmpty} from 'lodash';
export const ThemedLayUserInfo = (props) => {
  const {themedStyle, style, ...restProps} = props;
  const {user, strings, PressEdit} = restProps;
  const {username, displayName, image} = !isEmpty(user.data) && user.data;
  return (
    <Layout {...restProps} style={[style, styles.centered]}>
      <View style={styles.stretch}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.buttonright}
          onPress={restProps.PressSetting}>
          <UIIconFeather name="settings" size={25} />
        </TouchableOpacity>
      </View>
      <UIAvatar size={100} source={{uri: image}} />
      <UITitle
        title={username}
        type="small"
        nopadding
        top={10}
      />
      <UISubtitle
        title={displayName}
        type="small"
        top={2}
      />
      <View style={[styles.row, styles.centered, styles.mtop10]}>
        <View style={[styles.flex, styles.aligncenter]}>
          <UIIcon name="sword" size="small" width={37} height={32} />
          <UISubtitle
            title={strings('profile.text.follow', {name: '20'})}
            type="small"
            center={true}
            top={2}
            left={-10}
            small
          />
        </View>
        <View style={[styles.flex, styles.aligncenter]}>
          <UIIcon name="goal" size="small" width={30} height={30} />
          <UISubtitle
            title={strings('profile.text.win', {name: '20'})}
            type="small"
            top={2}
            small
          />
        </View>
      </View>
      <UIButton
        title={strings('profile.text.change-profile')}
        top={10}
        onPress={PressEdit}
      />
    </Layout>
  );
};
