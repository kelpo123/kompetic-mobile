import React from 'react';
import { ThemedDetailEvent } from './info.js';

export const LayDetailEvent = (props) => (
  <ThemedDetailEvent {...props} />
);