import React from 'react';

import {TouchableOpacity, View} from 'react-native';
import {withStyles} from '@ui-kitten/components';
import {UIIcon, UIIconFeather} from '@components/ui/icons';
import {UITitle, UISubtitle} from '@components/ui/typography';
import styles from '@assets/styles';
const info = (props) => {
  const {themedStyle, style, ...restProps} = props;
  const {data, strings} = restProps;
  return (
    <View {...restProps} style={[style]}>
      <UITitle title={data.name}  />
      <View style={[styles.column, styles.mtop5]}>
        <UISubtitle title={data.pendaftaran} small/>
      </View>

      <View style={[styles.row, styles.mtop20, styles.aligncenter]}>
        <View style={[styles.row, styles.flex]}>
            <View style={[styles.row, styles.aligncenter, styles.mright10]}>
            <UIIcon
                name={data.kategori_icon}
                width={25}
                height={25}
                right={10}
                contain
            />
            <UISubtitle title="Design" />
            </View>
            <View style={[styles.row, styles.aligncenter, styles.mright10]}>
            <UIIcon name={data.icon} width={25} height={25} right={10} contain />
            <UISubtitle title="Logo" />
            </View>
        </View>
        <View style={[styles.row, styles.aligncenter]}>
            <UIIconFeather name="users" size={20} right={5} top={-1}/>
            <UISubtitle title={data.people+"/"} black />
            <UISubtitle title={data.until} black />
        </View>
      </View>

      <View style={[styles.row, styles.mtop20]}>
        <UISubtitle title={data.desc} />
      </View>

      <View style={[styles.column, styles.mtop20]}>
        <UITitle title={strings('others.role')} small bot={5}/>
        {data.aturan.map((aturan, index) => (
        <View style={[styles.row, styles.aligncenter.center]}>
            <UISubtitle title={index+1+". "} small/>
            <UISubtitle title={aturan} small/>
        </View>
        ))}
      </View>

      <View style={[styles.column, styles.mtop20]}>
        <UITitle title={strings('others.date')} small bot={5}/>
        <UISubtitle title={data.date} small/>
      </View>

      <View style={[styles.column, styles.mtop20]}>
        <UITitle title={strings('others.gift')} small bot={5}/>
        {data.hadiah.map((hadiah, index) => (
        <View style={[styles.row, styles.aligncenter.center]}>
            <UISubtitle title={strings('others.winner')+" "+(index+1)+ " "} small/>
            <UISubtitle title={hadiah} small/>
        </View>
        ))}
      </View>

      <View style={[styles.column, styles.mtop20]}>
        <UITitle title={strings('others.ads-doc')} small bot={5}/>
        <TouchableOpacity onPress={() => alert("Open Pdf")}>
            <UISubtitle title={"Requirement.pdf"} red/>
        </TouchableOpacity>
      </View>

    </View>
  );
};

export const ThemedDetailEvent = withStyles(info, (theme) => ({
  button: {
    backgroundColor: theme['color-primary-500'],
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
}));
