import React from 'react';
import { ThemedRequireLogin } from './requireLogin';

export const LayRequireLogin = (props) => (
  <ThemedRequireLogin {...props} />
);
