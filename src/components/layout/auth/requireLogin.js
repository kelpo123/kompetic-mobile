import React from 'react';
import {Layout, Icon, Button, Text} from '@ui-kitten/components';
import {withStyles} from '@ui-kitten/components';
import styles from '@assets/styles';
const requireLogin = props => {
  const {themedStyle, style, ...restProps} = props;
  let {strings} = restProps;
  return (
    <Layout {...restProps} style={[style, styles.containercenter]}>
      <Icon name="iconic" pack="assets" width={140} height={140} />
      <Text
        style={[
          styles.textRegular,
          themedStyle.textBlack,
          styles.pv10,
          styles.mtop10,
        ]}>
        {strings('auth.require_login')}
      </Text>
      <Button
        style={[themedStyle.bgPrimary, styles.btn, styles.mbot10]}
        appearance="ghost"
        onPress={restProps.onPressMobile}
        icon={() => (
          <Icon
            {...style}
            name="smartphone"
            pack="feather"
            style={[themedStyle.textBlack, styles.iconButton]}
          />
        )}
        size="large"
        activeOpacity={0.8}
        textStyle={[themedStyle.textBlack]}>
        {strings('auth.phone.button')}
      </Button>
      <Button
        style={[themedStyle.bgFacebook, styles.btn]}
        onPress={restProps.onPressFacebook}
        appearance="ghost"
        icon={() => (
          <Icon
            {...style}
            name="facebook"
            pack="feather"
            style={[themedStyle.textWhite]}
          />
        )}
        size="large"
        activeOpacity={0.8}
        textStyle={[themedStyle.textWhite]}>
        {strings('auth.facebook.button')}
      </Button>
    </Layout>
  );
};

export const ThemedRequireLogin = withStyles(requireLogin, theme => ({
  bgPrimary: {
    backgroundColor: theme['color-primary-500'],
  },
  bgFacebook: {
    backgroundColor: theme['color-facebook'],
  },
  textBlack: {
    color: theme['color-black'],
  },
  textWhite: {
    color: theme['color-white'],
  },
}));
