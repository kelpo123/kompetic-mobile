import React from 'react';

import {TouchableOpacity, Text} from 'react-native';
import {Layout, Icon, withStyles} from '@ui-kitten/components';
import {UISubtitle} from '@components/ui/typography'
import styles from '@assets/styles';
const header = props => {
  const {themedStyle, style, ...restProps} = props;
  const {actionright, righttitle, onRightButtonEvent} = restProps;
  return (
    <Layout {...restProps} style={[style, styles.section]}>
      <Layout
        style={[styles.row, styles.aligncenter, styles.ph20]}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => restProps.navigation.goBack()}>
          <Icon
            name="chevron-left"
            pack="feather"
            style={[{fontSize:25}, styles.mright10]}
          />
        </TouchableOpacity>
        <Text style={[styles.titleheader, themedStyle.textBlack, styles.flex]}>
          {restProps.title}
        </Text>
        {actionright ?
        <TouchableOpacity
          activeOpacity={0.8}
          style={[ themedStyle.button]}
          onPress={onRightButtonEvent}>
          <UISubtitle title={righttitle} black small/>
        </TouchableOpacity> : null}
      </Layout>
    </Layout>
  );
};

export const ThemedHeader = withStyles(header, theme => ({
  textBlack: {
    color: theme['color-black'],
  },
  button:{
    backgroundColor: theme['color-primary-500'],
    borderRadius:10,
    paddingHorizontal:10,
    paddingVertical:5
  }
}));
