import React from 'react';
import { ThemedHeader } from './header.js';

export const LayHeader = (props) => (
  <ThemedHeader {...props} />
);