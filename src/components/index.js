import React from 'react';
import ThemedLoading from './extra/loading';
import ThemedBadge from './extra/badge';
import ThemedEmptyData from './extra/emptyData';
import ThemedTypography from './typography';
import ThemedTypographyShowAll from './typography/titleShowAll';
import ThemedIcon from './icons';
import ThemedIconFeather from './icons/iconFeather';
import ThemedEventList from './card/event';
import ThemedContainer from './basic/container';
import ThemedRow from './basic/row';
import ThemedColumn from './basic/column';
import ThemedButton from './basic/button';
import ThemedButtonIconAsset from './button/buttonIcon';

//extra
export const UILoading = props => <ThemedLoading {...props} />;
export const UIEmptyData = props => <ThemedEmptyData {...props} />;
export const UIBadge = props => <ThemedBadge {...props} />;

//typography
export const UITypography = props => <ThemedTypography {...props} />;
export const UITypographyShowAll = props => (
  <ThemedTypographyShowAll {...props} />
);

//icons
export const UIIcon = props => <ThemedIcon {...props} />;
export const UIIconFeather = props => <ThemedIconFeather {...props} />;

//button
export const UIButtonIconAsset = props => <ThemedButtonIconAsset {...props} />;

//card
export const UIEventList = props => <ThemedEventList {...props} />;

//basic
export const UIContainer = props => <ThemedContainer {...props} />;
export const UIRow = props => <ThemedRow {...props} />;
export const UIColumn = props => <ThemedColumn {...props} />;
export const UIButton = props => <ThemedButton {...props} />;
