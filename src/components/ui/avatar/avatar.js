import React from 'react';
import {Avatar} from '@ui-kitten/components';
export const ThemedAvatar = props => {
  const {themedStyle, style, ...restProps} = props;
  const size = restProps.size;
  return (
    <Avatar style={[style, {width:size, height:size}]} {...restProps} />
  );
};

