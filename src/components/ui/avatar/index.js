import React from 'react';
import {ThemedAvatar} from './avatar.js'
export const UIAvatar = (props) => (
  <ThemedAvatar {...props} />
);
