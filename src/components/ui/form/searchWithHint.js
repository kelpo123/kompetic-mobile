import React from 'react';
import {Input, Layout, Icon, withStyles, Popover} from '@ui-kitten/components';
import {Text, TouchableOpacity, View} from 'react-native';
import {UIAvatarAndName} from '@components/ui/card';
import {UISpinner} from '@components/ui/loading';
import styles from '@assets/styles';

const handleChoose = (data, closeFetching, addToList) => {
  closeFetching();
  addToList(data);
};

const PopoverContent = (data, loading, closeFetching, addToList) => (
  <Layout >
    {loading ? (
      <UISpinner title="sedang mencari.." />
    ) : (
      <View>
        <UIAvatarAndName
          onPress={() => handleChoose(data, closeFetching, addToList)}
          uri={data.uri}
          name={data.name}
          add
        />
      </View>
    )}
  </Layout>
);

const textInput = props => {
  const {themedStyle, style, ...restProps} = props;
  const {
    top,
    left,
    right,
    bot,
    data,
    onClickIcon,
    loading,
    fetching,
    addToList,
    closeFetching,
  } = restProps;
  return (
    <Popover
      {...restProps}
      visible={fetching}
      fullWidth={true}
      style={[
        themedStyle.cardresult,
        {
          marginTop: top,
          marginBottom: bot,
          marginLeft: left,
          marginRight: right,
        },
      ]}
      content={PopoverContent(data, loading, closeFetching, addToList)}
      onBackdropPress={closeFetching}>
      <View>
        <Input
          label={restProps.label}
          labelStyle={[styles.subtitlesmall, themedStyle.textSubBlack]}
          icon={
            restProps.name
              ? () => (
                  <TouchableOpacity activeOpacity={1} onPress={onClickIcon}>
                    <Icon
                      name={restProps.name}
                      pack="feather"
                      style={(themedStyle.textBlack, {height: 22})}
                    />
                  </TouchableOpacity>
                )
              : null
          }
          size={'large'}
          style={[styles.textInput]}
          placeholderTextColor={styles.textSubBlack}
          textStyle={[styles.subtitleMedium, themedStyle.textBlack]}
          status={restProps.error ? 'danger' : 'control'}
          caption={restProps.error ? restProps.errorMessage : null}
          {...restProps}
        />
      </View>
    </Popover>
  );
};

export const ThemedSearchWithHint = withStyles(textInput, theme => ({
  bgPrimary: {
    backgroundColor: theme['color-primary-500'],
  },
  textBlack: {
    color: theme['color-black'],
  },
  textSubBlack: {
    color: theme['color-subblack'],
  },
  cardresult: {
    borderRadius: 5,
    paddingVertical: 15,
    borderColor: theme['color-smooth'],
    elevation:1
  },

}));
