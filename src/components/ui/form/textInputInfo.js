import React from 'react';
import {withStyles, Input} from '@ui-kitten/components';
import {Text, TouchableOpacity, View} from 'react-native';
import {UICountdown} from '@components/ui/extra';
import styles from '@assets/styles';
const textInputInfo = (props) => {
  const {themedStyle, ...restProps} = props;
  const {
    error,
    errorMessage,
    countdown,
    label,
    strings,
    countdownOut,
    top,
    bot,
    left,
    right,
  } = restProps;
  return (
    <View
      style={{
        marginTop: top,
        marginBottom: bot,
        marginLeft: left,
        marginRight: right,
      }}>
      <Input
        label={label}
        labelStyle={[styles.subtitlesmall, themedStyle.textSubBlack]}
        size={'large'}
        style={[styles.textInput]}
        placeholderTextColor={styles.textSubBlack}
        textStyle={[styles.subtitleMedium, themedStyle.textBlack]}
        status={error ? 'danger' : 'control'}
        caption={error ? errorMessage : null}
        {...restProps}
      />
      <TouchableOpacity
        disabled={countdown ? true : false}
        style={[
          styles.textOverlayRight,
          themedStyle.bgPrimary,
          countdown ? themedStyle.buttondisable : null,
        ]}
        {...restProps}>
        {!countdown ? (
          <Text style={[themedStyle.textBlack, styles.textButtonInfo]}>
            {strings('auth.phone.form.verify-code')}
          </Text>
        ) : (
          <UICountdown onFinish={() => countdownOut()} until={59} />
        )}
      </TouchableOpacity>
    </View>
  );
};

export const ThemedTextInputInfo = withStyles(textInputInfo, (theme) => ({
  bgPrimary: {
    backgroundColor: theme['color-primary-500'],
  },
  textBlack: {
    color: theme['color-black'],
  },
  buttondisable: {
    backgroundColor: theme['color-primary-disable'],
  },
}));
