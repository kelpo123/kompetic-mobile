import React from 'react';
import {Input, Layout, Icon, withStyles} from '@ui-kitten/components';
import {Text, TouchableOpacity} from 'react-native';
import {UIIcon, UIIconFeather} from '@components/ui/icons'
import styles from '@assets/styles';
const textInput = props => {
  const {themedStyle, style, ...restProps} = props;
  const {name, top, left, right, bot, onClickIcon} = restProps;
  return (
    <Layout
      style={[
        style,
        styles.stretch,
        {
          marginTop: top,
          marginLeft: left,
          marginBottom: bot,
          marginRight: right,
        },
      ]}
      {...restProps}>
      <Input
        label={restProps.label}
        labelStyle={[styles.subtitlesmall, themedStyle.textSubBlack]}
        icon={
          restProps.name
            ? () => (
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={onClickIcon}>
                  <UIIconFeather name={name} size={20} />
                </TouchableOpacity>
              )
            : null
        }
        size={'large'}
        style={[styles.textInput]}
        placeholderTextColor={styles.textSubBlack}
        textStyle={[
          styles.subtitleMedium,
          themedStyle.textBlack,
          restProps.phone ? {marginLeft: 80} : null,
        ]}
        status={restProps.error ? 'danger' : 'control'}
        caption={restProps.error ? restProps.errorMessage : null}
        {...restProps}
      />
      {restProps.phone ? (
        <Layout
          style={[styles.iconInput, styles.aligncenter, styles.textInput]}>
          <UIIcon name="flag-id" width={35} height={20} contain/>
          <Text
            style={[
              styles.subtitleMedium,
              themedStyle.textBlack,
              styles.mleft5,
            ]}>
            +62
          </Text>
        </Layout>
      ) : null}
    </Layout>
  );
};

export const ThemedTextInput = withStyles(textInput, theme => ({
  bgPrimary: {
    backgroundColor: theme['color-primary-500'],
  },
  textBlack: {
    color: theme['color-black'],
  },
  textSubBlack: {
    color: theme['color-subblack'],
  },
}));
