import React from 'react';
import { ThemedTextInput } from './textInput';
import {ThemedSearchWithHint} from './searchWithHint'
import {ThemedTextInputInfo} from './textInputInfo'
export const UITextInput = (props) => (
  <ThemedTextInput {...props} />
);

export const UITextInputInfo = (props) => (
  <ThemedTextInputInfo {...props} />
);

export const UISearchWithHint = (props) => (
  <ThemedSearchWithHint {...props} />
)
