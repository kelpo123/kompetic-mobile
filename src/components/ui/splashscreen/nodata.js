import React from 'react';
import {View, Text} from 'react-native';
export const ThemedNodata = props => {
  const {themedStyle, style, ...restProps} = props;
  const {title} = restProps
  return(
  <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
    <Text>{title}</Text>
  </View>
  );
};
