import React from 'react';
import { ThemedNodata } from './nodata';
export const UINodata = (props) => (
  <ThemedNodata {...props} />
);

