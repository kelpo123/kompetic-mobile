import React, {memo} from 'react';
import styles from '@assets/styles';
import {UITypography, UIButton, UIIcon} from '@components/ui';
import {setMargin} from '../global-func';
const ThemedButtonIcon = props => {
  const {name, title, style, ...rest} = props;
  return (
    <UIButton
      {...rest}
      style={[style, styles.aligncenter, {...setMargin(props)}]}
      activeOpacity={0.8}>
      <UIIcon name={name} width={60} height={60} />
      <UITypography title={title} top={5} />
    </UIButton>
  );
};

export default memo(ThemedButtonIcon);
