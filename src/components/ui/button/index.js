import React from 'react';
import { ThemedSubmitButton } from './submit';
import { ThemedSmallButton } from './button';
import { ThemedButtonText } from './buttonText';
import { ThemedButtonIconAsset } from './buttonIcon';

export const UISubmitButton = (props) => (
  <ThemedSubmitButton {...props} />
);

export const UIButton = (props) => (
  <ThemedSmallButton {...props} />
);

export const UIButtonText = (props) => (
  <ThemedButtonText {...props} />
);

export const UIButtonIconAsset = (props) => (
  <ThemedButtonIconAsset {...props} />
);

