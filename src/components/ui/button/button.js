import React from 'react';
import {withStyles} from '@ui-kitten/components';
import styles from '@assets/styles';
import {TouchableOpacity, Text, ActivityIndicator} from 'react-native';
const submitButton = props => {
  const {themedStyle, style, ...restProps} = props;
  const {top, bot, left, right} = restProps;
  return (
    <TouchableOpacity
      {...restProps}
      style={[
        style,
        styles.btn,
        themedStyle.bgPrimary,
        {marginTop:top, marginBottom:bot, marginLeft:left, marginRight:right}
      ]}
      activeOpacity={0.8}>
      {restProps.loading ? (
        <ActivityIndicator
          width="50"
          height="50"
          color={themedStyle.iconSpinner}
        />
      ) : (
        <Text
          style={[
            themedStyle.textBlack,
            styles.textcenter,
            styles.textButtonInfo
          ]}>
          {restProps.title}
        </Text>
      )}
    </TouchableOpacity>
  );
};

export const ThemedSmallButton = withStyles(submitButton, theme => ({
  bgPrimary: {
    backgroundColor: theme['color-primary-500'],
  },
  textBlack: {
    color: theme['color-black'],
  },
  subBlack: {
    color: theme['color-subblack'],
  },
  iconSpinner: theme['color-black'],
}));
