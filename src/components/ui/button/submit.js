import React from 'react';
import {withStyles} from '@ui-kitten/components';
import styles from '@assets/styles';
import {TouchableOpacity, Text, ActivityIndicator} from 'react-native';
const submitButton = props => {
  const {themedStyle, style, ...restProps} = props;
  return (
    <TouchableOpacity
      {...restProps}
      style={[
        themedStyle.bgPrimary,
        styles.btnbottom,
        restProps.disabled ? styles.disabled : styles.active,
      ]}
      activeOpacity={0.8}
      disabled={restProps.disabled ? true : false}>
      {restProps.loading ? (
        <ActivityIndicator
          width="50"
          height="50"
          color={themedStyle.iconSpinner}
        />
      ) : (
        <Text
          style={[
            restProps.disabled ? themedStyle.subBlack : themedStyle.textBlack,
            styles.textcenter,
            styles.textButton,
          ]}>
          {restProps.title}
        </Text>
      )}
    </TouchableOpacity>
  );
};

export const ThemedSubmitButton = withStyles(submitButton, theme => ({
  bgPrimary: {
    backgroundColor: theme['color-primary-500'],
  },
  textBlack: {
    color: theme['color-black'],
  },
  subBlack: {
    color: theme['color-subblack'],
  },
  iconSpinner: theme['color-black'],
}));
