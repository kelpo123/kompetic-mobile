import React from 'react';
import {withStyles} from '@ui-kitten/components';
import styles from '@assets/styles';
import {UIIconFeather} from '@components/ui/icons';
import {UITitle} from '@components/ui/typography'
import {TouchableOpacity} from 'react-native';
const buttonText = (props) => {
  const {themedStyle, style, ...restProps} = props;
  const {title, withIcon, name} = restProps;
  return (
    <TouchableOpacity
      {...restProps}
      style={[style, styles.sectionsmall, styles.row, styles.aligncenter]}
      activeOpacity={0.8}>
      {withIcon && (
        <UIIconFeather name={name} size={18}  />
      )}
      <UITitle title={title} small left={10}/>
    </TouchableOpacity>
  );
};

export const ThemedButtonText = withStyles(buttonText, (theme) => ({
  textBlack: {
    color: theme['color-black'],
  },
}));
