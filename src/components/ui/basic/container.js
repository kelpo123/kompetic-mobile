import React, {memo} from 'react';
import {StyleSheet, View} from 'react-native';
const ThemedContainer = props => {
  const {children, top, style, ...rest} = props;
  return (
    <View {...rest} style={[style, styles.container, {marginTop: top}]}>
      {children}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
  },
});

export default memo(ThemedContainer);
