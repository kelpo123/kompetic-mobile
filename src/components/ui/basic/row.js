import React, {memo} from 'react';
import {StyleSheet, View} from 'react-native';
const ThemedRow = props => {
  const {children, style, ...rest} = props;
  return (
    <View {...rest} style={[style, styles.row]}>
      {children}
    </View>
  );
};
const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
});

export default memo(ThemedRow);
