import React, {memo} from 'react';
import {TouchableOpacity} from 'react-native';
const ThemedButton = props => {
  const {children, style, ...rest} = props;
  return (
    <TouchableOpacity activeOpacity={0.9} {...rest} style={[style]}>
      {children}
    </TouchableOpacity>
  );
};
export default memo(ThemedButton);
