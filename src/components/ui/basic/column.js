import React, {memo} from 'react';
import {StyleSheet, View} from 'react-native';
const ThemedColumn = props => {
  const {children, style, ...rest} = props;
  return (
    <View {...rest} style={[style, styles.column]}>
      {children}
    </View>
  );
};
const styles = StyleSheet.create({
  column: {
    flexDirection: 'column',
  },
});

export default memo(ThemedColumn);
