import React from 'react';
import { ThemedAvatarName } from './avatarAndName';
import {ThemedEventList} from './event'

export const UIAvatarAndName = (props) => (
  <ThemedAvatarName {...props} />
);

export const UIEventList = (props) => (
  <ThemedEventList {...props} />
);

