import React from 'react';
import {withStyles} from '@ui-kitten/components';
import {UITitle, UISubtitle} from '@components/ui/typography';
import {UIIconFeather} from '@components/ui/icons'
import {UIAvatar} from '@components/ui/avatar';
import {TouchableOpacity, View} from 'react-native';
import styles from '@assets/styles';
export const avatarname = props => {
  const {themedStyle, style, ...restProps} = props;
  const {uri, name, top, bot, left, right, add, remove, onRemove} = restProps;
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={[
        styles.row,
        styles.aligncenter,
        styles.container,
        {
          marginTop: top,
          marginLeft: left,
          marginRight: right,
          marginBottom: bot,
        },
      ]}
      {...restProps}>
      <View style={[styles.flex, styles.row, styles.aligncenter]}>
        <UIAvatar size={30} source={{uri: uri}} />
        <UITitle title={name} smaller left={10} />
      </View>
      {add ?
      <View style={themedStyle.status}>
        <UISubtitle title="Tambah" white smaller />
      </View> :
      remove ?
      <TouchableOpacity onPress={onRemove} activeOpacity={0.6}>
        <UIIconFeather name="trash" size={24} style={themedStyle.red}/>
      </TouchableOpacity> : null
      }
    </TouchableOpacity>
  );
};

export const ThemedAvatarName = withStyles(avatarname, theme => ({
  status:{
    backgroundColor:theme['color-success-600'],
    borderRadius:5,
    paddingVertical:5,
    paddingHorizontal:15,
  },
  red:{
    color: theme['color-danger-600']
  }
}));
