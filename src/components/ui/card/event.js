import React, {memo} from 'react';
import {List} from '@ui-kitten/components';
import styles from '@assets/styles';
import {View} from 'react-native';
import {
  UIIcon,
  UIIconFeather,
  UITypography,
  UIButton,
  UIRow,
  UIColumn,
} from '@components/ui';
import {money} from '@components/functions/convert/number';
const themedEventCard = props => {
  const {themedStyle, style, ...restProps} = props;
  const {horizontal, strings, navigation} = restProps;
  const renderItem = ({item, index}) => (
    <UIButton
      onPress={() => navigation.navigate('DetailEvent')}
      key={index}
      style={[styles.cardhorizontal, horizontal && styles.mh10]}>
      <UIRow>
        <UIRow style={[styles.aligncenter, styles.flex]}>
          <UIIcon
            name={item.kategori_icon}
            width={25}
            height={25}
            right={10}
            contain
          />
          <UIIcon name={item.icon} width={25} height={25} contain />
        </UIRow>
        <UIRow style={[styles.aligncenter]}>
          <UIIconFeather name="users" size={15} right={3} top={-1} />
          <UITypography title={item.people + '/'} />
          <UITypography title={item.until} />
        </UIRow>
      </UIRow>

      <UIColumn style={styles.flex}>
        <UITypography title={item.name} top={10} />
        <UITypography title={item.pendaftaran} top={2} />
      </UIColumn>

      <UIRow>
        <UIColumn style={styles.flex}>
          <UITypography title={strings('others.regis-fee')} />
          <UITypography title={item.fee} />
        </UIColumn>
        <UIColumn style={styles.column}>
          <UITypography title={strings('others.total-prize')} />
          <UITypography title={money.formatRupiah(item.price)} />
        </UIColumn>
      </UIRow>
    </UIButton>
  );

  return (
    <View style={horizontal && {height: 200}}>
      <List
        {...restProps}
        renderItem={renderItem}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={[horizontal && styles.ph15]}
      />
    </View>
  );
};

export default memo(themedEventCard);
