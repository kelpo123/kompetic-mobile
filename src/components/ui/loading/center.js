import React from 'react';
import {Spinner, withStyles} from '@ui-kitten/components';
import {UISubtitle} from '@components/ui/typography'
import {View} from 'react-native';
export const spinner = props => {
  const {themedStyle, style, ...restProps} = props;
  const {title, middle} = restProps;
  const layout = middle ? themedStyle.middle : themedStyle.center;
  return(
  <View style={layout}>
    <Spinner {...restProps}/>
    {title ?
    <UISubtitle title={title} /> : null
    }
  </View>
  );
};

export const ThemedSpinner = withStyles(spinner, theme => ({
  center:{
    alignItems:'center'
  },
  middle:{
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  }
}));
