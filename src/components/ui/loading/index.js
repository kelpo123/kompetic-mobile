import React from 'react';
import { ThemedSpinner } from './center';
export const UISpinner = (props) => (
  <ThemedSpinner {...props} />
);

