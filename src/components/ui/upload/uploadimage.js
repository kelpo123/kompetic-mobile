import React from 'react';
import {TouchableOpacity, View, Image} from 'react-native';
import {UISubtitle} from '@components/ui/typography';
import {UIIconFeather} from '@components/ui/icons';
import {withStyles} from '@ui-kitten/components';
import styles from '@assets/styles';
export const uploadImage = props => {
  const {themedStyle, style, ...restProps} = props;
  const {title, top, bot, left, right, image, rounded} = restProps;
  const imageStyle = rounded ? themedStyle.imagerounded : themedStyle.image;
  return (
    <View>
      {image ? (
        <TouchableOpacity {...restProps} activeOpacity={0.8} style={styles.aligncenter}>
          <Image source={{uri: image}} style={[imageStyle, {
              alignItems: 'center',
              marginTop: top,
              marginBottom: bot,
              marginLeft: left,
              marginRight: right,
            }]}/>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          {...restProps}
          activeOpacity={0.8}
          style={[
            style,
            themedStyle.bordered,
            {
              alignItems: 'center',
              marginTop: top,
              marginBottom: bot,
              marginLeft: left,
              marginRight: right,
            },
          ]}>
          <UIIconFeather name="image" size={40} soft />
          <UISubtitle title={title} top={10} />
        </TouchableOpacity>
      )}
    </View>
  );
};

export const ThemedUploadImage = withStyles(uploadImage, theme => ({
  bordered: {
    borderWidth: 2,
    borderRadius: 15,
    borderColor: theme['color-smooth'],
    borderStyle: 'dotted',
    paddingVertical: 15,
  },
  image: {
    height:180
  },
  imagerounded: {
    height:120,
    width:120,
    borderRadius:200,
  }
}));
