import React from 'react';
import { ThemedUploadImage } from './uploadimage';
export const UIUploadImage = (props) => (
  <ThemedUploadImage {...props} />
);

