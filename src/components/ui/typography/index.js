import React, {memo} from 'react';
import {Text} from 'react-native';
import {setColor, setTypography, setMargin} from '../global-func';
const ThemedTypography = props => {
  const {themedStyle, title, size, type, color, style, ...rest} = props;
  return (
    <Text
      {...rest}
      style={[
        style,
        {
          fontSize: size,
          fontFamily: setTypography(type),
          color: setColor(color),
          ...setMargin(props),
        },
      ]}>
      {title}
    </Text>
  );
};

export default memo(ThemedTypography);
