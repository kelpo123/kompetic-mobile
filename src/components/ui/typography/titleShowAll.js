import React, {memo} from 'react';
import {View} from 'react-native';
import {UITypography, UIRow, UIButton} from '@components/ui';
import styles from '@assets/styles';
import {setMargin} from '../global-func';

const ThemedTitleWithShowAll = props => {
  const {themedStyle, ...restProps} = props;
  const {title, strings} = restProps;
  return (
    <UIRow style={[styles.aligncenter, ...setMargin(props)]}>
      <View style={styles.flex}>
        <UITypography title={title} />
      </View>
      <UIButton {...restProps}>
        <UITypography title={strings('others.show-all')} color="primary" />
      </UIButton>
    </UIRow>
  );
};

export default memo(ThemedTitleWithShowAll);
