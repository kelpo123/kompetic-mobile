import React from 'react';
import {withStyles} from '@ui-kitten/components';
import styles from '@assets/styles';
import Spinner from 'react-native-loading-spinner-overlay';
const spinnerUI = props => {
  const {themedStyle, style, ...restProps} = props;
  const {loading} = restProps;
  return (
    <Spinner
        {...restProps}
          visible={loading}
          textContent={'Loading...'}
          overlayColor={"rgba(0, 0, 0, 0.45)"}
          textStyle={styles.subtitle, themedStyle.textWhite, {marginTop:-50}}
        />
  );
};

export const ThemedSpinner = withStyles(spinnerUI, theme => ({
  textWhite: {
    color: theme['color-white'],
  },
}));
