import React from 'react';
import {withStyles} from '@ui-kitten/components';
import styles from '@assets/styles';
import TimerCountdown from '@components/functions/time/countdown';
const countdowntime = props => {
  const {themedStyle, style, ...restProps} = props;
  return (
    <TimerCountdown
         {...restProps}
         timeToShow={['S']}
         timeLabels={{s: null}}
         digitStyle={themedStyle.countdowndigit}
         digitTxtStyle={[
            themedStyle.textBlack,  styles.textButtonInfo, themedStyle.countdown
          ]}
    />
  );
};

export const ThemedCountDown = withStyles(countdowntime, theme => ({
   textBlack: {
    color: theme['color-black'],
  },
  countdowndigit:{
    background: 'trasparent',
    height:25
  }
}));
