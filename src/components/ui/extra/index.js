import React from 'react';
import { ThemedCountDown } from './countdown';
import {ThemedSpinner} from './spinner.js'
export const UICountdown = (props) => (
  <ThemedCountDown {...props} />
);

export const UISpinner = (props) => (
    <ThemedSpinner {...props} />
  );
