import React, {memo} from 'react';
import {Icon} from '@ui-kitten/components';
import {View} from 'react-native';
import {setMargin} from '../global-func';
const ThemedIcon = props => {
  const {style, contain, name, width, height} = props;
  const mode = contain ? 'contain' : 'cover';
  return (
    <View style={[style, ...setMargin(props)]}>
      <Icon
        name={name}
        height={height}
        width={width}
        pack="assets"
        resizeMode={mode}
      />
    </View>
  );
};

export default memo(ThemedIcon);
