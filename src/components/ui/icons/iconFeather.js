import React, {memo} from 'react';
import {Icon} from '@ui-kitten/components';
import {setMargin} from '../global-func';
const ThemedIconFeather = props => {
  const {color, themedStyle, style, ...rest} = props;
  return (
    <Icon
      {...rest}
      pack="feather"
      style={[style, {color: setColor(color), ...setMargin(props)}]}
    />
  );
};

export default memo(ThemedIconFeather);
