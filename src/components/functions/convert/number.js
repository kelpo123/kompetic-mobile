export const money = {
    formatRupiah
};

function formatRupiah(num) {
  const val = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  const prefixRp = "Rp"+val;
  return prefixRp;
}
