import {NavigationActions, StackActions} from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  /** OPTIONAL you can use this navigation object */
  const navigation = _navigator._navigation;
  navigation.navigate({routeName, params});
  // _navigator.props.dispatch(
  //   NavigationActions.navigate({
  //     routeName,
  //     params,
  //   })
  // );
}

function goBack() {
  const navigation = _navigator._navigation;
  navigation.goBack();
}

function dispatch(action) {
  const navigation = _navigator._navigation;
  navigation.dispatch(action);
}

function pop(n) {
  const navigation = _navigator._navigation;

  navigation.pop(n);
}

function tab(routeName, params) {
  const navigation = _navigator._navigation;
  let resetAction = StackActions.reset({
    index: 0,
    key: null, // required to reset tabs
    actions: [
      NavigationActions.navigate({
        routeName: 'Home',
        params,
        action: NavigationActions.navigate({routeName, params}),
      }),
    ],
  });
  navigation.dispatch(resetAction);
}

function popToTop() {
  const navigation = _navigator._navigation;
  navigation.popTopTop();
}

function replace(routeName, params) {
  const replaceAction = StackActions.replace({
    routeName,
    params,
  });
  this.dispatch(replaceAction);
}

function findActiveScreen(state) {
  const {routes, index} = state;
  if (routes && routes[index]) {
    return findActiveScreen(routes[index]);
  } else {
    return state;
  }
}

function getActiveScreenAndParams() {
  const navigation = _navigator.currentNavProp;
  const {state} = navigation;
  return findActiveScreen(state, null);
}

// add other navigation functions that you need and export them

export default {
  navigate,
  goBack,
  dispatch,
  tab,
  pop,
  popToTop,
  replace,
  setTopLevelNavigator,
  getActiveScreenAndParams,
};
