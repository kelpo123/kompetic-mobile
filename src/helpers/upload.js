import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import {ToastAndroid} from 'react-native'
import firebase from 'react-native-firebase';

export async function setImage() {
    let data={uri:''};
    const options = {
          title: 'Select Avatar',
          mediaType: 'photo',
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        data =  await ImagePicker.launchImageLibrary(options, response => {
        if (response.error) {
          ToastAndroid.show(response.error, ToastAndroid.LONG);
        }
        else if (!response.didCancel) {
             return {
              data:{
                uri: response.uri
              }
            }
        }
        else {
            return
        }
    });
    return data;
};

export async function resizeImage(photo, width, height, ext, id) {
  let data={
    image: '',
    uri:'',
  };
  data = await ImageResizer.createResizedImage(
    photo,
    width,
    height,
    ext,
    100,
  ).then(compressedImage => {
    const ext = compressedImage.uri.split('.').pop();
    const image = `${id}.${ext}`;
    return {
      data:{
        image:image,
        uri:compressedImage.uri
      }
    }
  });
  return data
}

export async function uploadImage(name, uri) {
  let image;
  await firebase
      .storage()
      .ref(`${'user-image'}/${name}`)
      .putFile(uri)
      .on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      snapshot => {
        if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
          image = snapshot.downloadURL;
          return image;
        }
      },
      error => {
        unsubscribe();
        ToastAndroid.show("Upload image gagal, coba sekali lagi!", ToastAndroid.LONG);
        return
      },
    );
}