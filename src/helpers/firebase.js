import firebase from 'react-native-firebase';
import {AccessToken, LoginManager} from 'react-native-fbsdk';
import {ToastAndroid} from 'react-native';

export function loginPhone(number) {
  return firebase.auth().signInWithPhoneNumber('+62' + number);
}

export async function LoginFacebook() {
  const result = await LoginManager.logInWithPermissions([
    'public_profile',
    'email',
  ]);
  if (result.isCancelled) {
    return Promise.reject({type: 'cancel'});
  }

  const data = await AccessToken.getCurrentAccessToken();

  if (!data) {
    ToastAndroid.show(
      'Terjadi ganguan pada provider facebook',
      ToastAndroid.LONG,
    );
    return;
  }
  return (credential = firebase.auth.FacebookAuthProvider.credential(
    data.accessToken,
  ));
}

export function LoginCredential(credential) {
  return firebase.auth().signInWithCredential(credential);
}

export function getTokenFirebase() {
  return firebase.auth().currentUser.getIdToken(true);
}

export function getFcmToken() {
  return firebase.messaging().getToken();
}
