import axios from 'axios';
import handleError from '@helpers/handleError';
import { BASE_URL_API } from 'react-native-dotenv';
import AsyncStorage from '@react-native-community/async-storage';

const getInstance = (port) => {
  const instance = axios.create({
    baseURL: BASE_URL_API+":"+port,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/vnd.api+json',
    },
  });

  instance.interceptors.request.use(
    async (config) => {
      const newConfig = config;
      const token = await AsyncStorage.getItem('Token')
      if (token) {
        newConfig.headers.Authorization = `${token}`;
      }
      return newConfig;
    },
    error => Promise.reject(error),
  );

  return instance;
};

const httpRequest = (method, port, path, data, headers = {}) => {
  return new Promise((resolve, reject) => {
    getInstance(port)[method](path, data, headers)
      .then(response => resolve(response))
      .catch((error) => reject(handleError(error)));
  });
};

export default {
  get(port, path, data, headers) {
    return httpRequest('get', port, path, data, headers);
  },
  post(port, path, data, headers) {
    return httpRequest('post', port, path, data, headers);
  },
  put(port, path, data, headers) {
    return httpRequest('put', port, path, data, headers);
  },
  delete(port, path, data, headers) {
    return httpRequest('delete', port, path, data, headers);
  },
  patch(port, path, data, headers) {
    return httpRequest('patch', port, path, data, headers);
  },
};
