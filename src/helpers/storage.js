import AsyncStorage from '@react-native-community/async-storage';

export const storage = {
    sendToken,
    sendUser,
    getToken,
    getUser,
    removeAll,
    removeToken
  };

function sendToken(token){
    AsyncStorage.setItem('Token', token)
}

function sendUser(data){
    AsyncStorage.setItem('User', JSON.stringify(data))
}

async function getToken (){
    const token = await AsyncStorage.getItem('Token')
    return token;
}

async function getUser(){
    var user = await AsyncStorage.getItem('User')
    var dataUser = JSON.parse(user);
    return dataUser;
}

function removeAll(){
    AsyncStorage.removeItem('Token')
    AsyncStorage.removeItem('User')
}
function removeToken(){
    AsyncStorage.removeItem('Token')
}
