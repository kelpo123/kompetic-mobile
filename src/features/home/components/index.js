import React from 'react';

import {SafeAreaView} from 'react-native';
import {
  UITypography,
  UITitleWithShowAll,
  UIButtonIconAsset,
  UIEventList,
  UIContainer,
  UIRow,
} from '@components/ui';
import styles from '@assets/styles';
import {EventList, DesignList} from '@constants/dummy';

const index = props => {
  const {navigation, strings} = props;
  return (
    <SafeAreaView style={[styles.wrapper]}>
      <UIContainer>
        <UITypography title="Kompetic" top={20} />
        <UIRow style={styles.mtop20}>
          <UIButtonIconAsset
            title={strings('team.title')}
            name="team"
            right={30}
            onPress={() => navigation.push('Team')}
          />
          <UIButtonIconAsset
            title={strings('leaderboard.title')}
            name="leaderboard"
            right={30}
          />
          <UIButtonIconAsset
            title={strings('reward.title')}
            name="archievment"
          />
        </UIRow>
      </UIContainer>

      <UIContainer>
        <UITitleWithShowAll
          top={20}
          strings={strings}
          title="Rekomendasi Kompetisi"
          onPress={() => alert('this')}
        />
      </UIContainer>
      <UIEventList
        data={EventList}
        horizontal={true}
        strings={strings}
        navigation={navigation}
      />

      <UIContainer>
        <UITitleWithShowAll
          top={20}
          strings={strings}
          title="Design"
          onPress={() => alert('this')}
        />
      </UIContainer>
      <UIEventList
        data={DesignList}
        horizontal={true}
        strings={strings}
        navigation={navigation}
      />
    </SafeAreaView>
  );
};

export default index;
