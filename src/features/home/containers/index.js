import { connect } from "react-redux";
import {requestGetUser} from "@redux/actions/user"
import Index from "../components";

function mapStateToProps(state){
  const {user} = state.user;
  return{
    user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getUser: () => dispatch(requestGetUser()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Index);
