import { connect } from "react-redux";
import {requestLogout} from "@redux/actions/auth"
import Index from "../components";

function mapStateToProps(state){
  const {user} = state.user;
  return{
    user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(requestLogout()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Index);
