import React from 'react';
import {Text, SafeAreaView} from 'react-native';
import RequireLogin from '@features/auth/container/requireLogin';

const index = props => {
  const {user} = props;
  let {strings} = props.screenProps;
  if (!user.data) {
    return <RequireLogin strings={strings} loading={user.loading} />;
  } else {
    return (
      <SafeAreaView style={{flex: 1}}>
        <Text>{user.data && user.data.displayName}</Text>
      </SafeAreaView>
    );
  }
};

export default index;
