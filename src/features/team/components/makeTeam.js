import React, {Component} from 'react';
import {SafeAreaView, View, ToastAndroid} from 'react-native';
import {remove, find} from 'lodash';
import RequireLogin from '@features/auth/container/requireLogin';
import {LayHeader} from '@components/layout/header';
import {UITextInput, UISearchWithHint} from '@components/ui/form';
import {UIUploadImage} from '@components/ui/upload';
import {UISubmitButton} from '@components/ui/button';
import {UIAvatarAndName} from '@components/ui/card';
import {Result} from '@constants/dummy';
import ImagePicker from 'react-native-image-picker';
import styles from '@assets/styles';
class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        username: [],
        name: '',
        image: '',
      },
      isFetchingResult: false,
      loadingResult: false,
      loading: false,
    };
  }

  async handleUpload() {
    const {form} = this.state;
    const options = {
      title: 'Select Avatar',
      mediaType: 'photo',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    await ImagePicker.launchImageLibrary(options, response => {
      if (response.error) {
        ToastAndroid.show(response.error, ToastAndroid.LONG);
      } else if (!response.didCancel) {
        this.setState({
          form: {
            ...form,
            image: response.uri,
          },
        });
      } else {
        return;
      }
    });
  }

  handleChange(name, value) {
    const {form} = this.state;
    this.setState({
      form: {
        ...form,
        [name]: value,
      },
    });
  }

  handleAddUser(data) {
    const {
      form: {username},
    } = this.state;
    const notvalid = username.find(obj => obj.id === data.id);
    if (notvalid) {
      ToastAndroid.show('username sudah terdaftar', ToastAndroid.LONG);
    } else {
      username.push(data);
    }
  }

  handleRemoveUser(data) {
    const {
      form: {username},
    } = this.state;
    remove(username, obj => obj.id === data.id);
    this.setState({username});
  }

  findResult() {
    this.setState({isFetchingResult: true, loadingResult: true});
    setTimeout(() => {
      this.setState({loadingResult: false});
    }, 1000);
  }

  render() {
    const {user, navigation} = this.props;
    const {
      loading,
      isFetchingResult,
      loadingResult,
      form: {name, image, username},
    } = this.state;
    let {strings} = this.props.screenProps;
    if (!user.data) {
      return <RequireLogin strings={strings} loading={user.loading} />;
    } else {
      return (
        <SafeAreaView style={{flex: 1}}>
          <LayHeader
            navigation={navigation}
            title={strings('team.make-team')}
          />
          <View style={[styles.container, styles.mtop20]}>
            <UIUploadImage
              title={strings('team.form.upload')}
              bot={20}
              image={image}
              onPress={() => this.handleUpload()}
              rounded
            />
            <UITextInput
              placeholder={strings('team.form.name-placeholder')}
              label={strings('team.form.name-team')}
              bot={10}
              value={name}
              onChangeText={value => this.handleChange('name', value)}
            />
            <UISearchWithHint
              placeholder={strings('team.form.partner-placeholder')}
              label={strings('team.form.partner')}
              name="search"
              onClickIcon={() => this.findResult()}
              onSubmitEditing={() => this.findResult()}
              loading={loadingResult}
              fetching={isFetchingResult}
              returnKeyType="search"
              data={Result}
              addToList={data => this.handleAddUser(data)}
              closeFetching={() => this.setState({isFetchingResult: false})}
            />
          </View>
          {username.map((data, key) => (
            <UIAvatarAndName
              top={10}
              bot={10}
              key={key}
              uri={data.uri}
              name={data.name}
              onRemove={() => this.handleRemoveUser(data)}
              remove
            />
          ))}
          <UISubmitButton
            title={strings('team.form.submit')}
            loading={loading}
            onPress={() => this.handleSubmit()}
          />
        </SafeAreaView>
      );
    }
  }
}
export default Index;
