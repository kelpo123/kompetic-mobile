/* eslint-disable react-hooks/rules-of-hooks */
import React, {useEffect} from 'react';
import {Text, SafeAreaView} from 'react-native';
import RequireLogin from '@features/auth/container/requireLogin';
import {LayHeader} from '@components/layout/header';
import {UISpinner} from '@components/ui/loading';
import {UINodata} from '@components/ui/splashscreen';

const index = props => {
  let {strings} = props.screenProps;
  const {getTeam, team, user, navigation} = props;
  useEffect(() => {
    getTeam();
  }, [getTeam]);
  if (!user.data) {
    return <RequireLogin strings={strings} loading={user.loading} />;
  } else {
    return (
      <SafeAreaView style={{flex: 1}}>
        <LayHeader
          navigation={navigation}
          title={strings('team.title')}
          righttitle={strings('team.make-team')}
          onRightButtonEvent={() => navigation.push('MakeTeam')}
          actionright
        />
        {team.loading ? (
          <UISpinner />
        ) : team.data ? (
          <Text>{user.data && user.data.displayName}</Text>
        ) : (
          <UINodata title="anda tidak memiliki teams" />
        )}
      </SafeAreaView>
    );
  }
};

export default index;
