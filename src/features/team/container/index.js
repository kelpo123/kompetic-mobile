import { connect } from "react-redux";
import {getTeam} from "@redux/actions/team"
import Index from "../components";

function mapStateToProps(state){
  const {team} = state.team;
  const {user} = state.user;
  return{
    team,
    user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getTeam: () => dispatch(getTeam()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Index);
