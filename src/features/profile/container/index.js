import { connect } from "react-redux";
import Index from "../components";

function mapStateToProps(state){
  const {user} = state.user;
  return{
    user
  }
}


export default connect(mapStateToProps)(Index);
