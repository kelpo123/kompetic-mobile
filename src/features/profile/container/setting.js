import { connect } from "react-redux";
import {requestLogout} from "@redux/actions/auth"
import Setting from "../components/setting.js";

function mapStateToProps(state){
  const {user} = state.user;
  return{
    user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    logout: (navigation) => dispatch(requestLogout(navigation)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Setting);
