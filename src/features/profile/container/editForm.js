import { connect } from "react-redux";
import {requestLogout} from "@redux/actions/auth"
import {requestEdit, requestLoading} from "@redux/actions/user"
import EditForm from "../components/editForm.js";

function mapStateToProps(state){
  const {user} = state.user;
  return{
    user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    editProfile: (name, status, navigation) => dispatch(requestEdit(name, status, navigation)),
    loading: () => dispatch(requestLoading()),
    logout: (navigation) => dispatch(requestLogout(navigation)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditForm);
