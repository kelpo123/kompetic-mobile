import React, {Component} from 'react';
import {SafeAreaView, TouchableOpacity, View} from 'react-native';
import styles from '@assets/styles';
import ImagePicker from 'react-native-image-picker';
import {Layout} from '@ui-kitten/components';
import {LayHeader} from '@components/layout/header';
import {UIAvatar} from '@components/ui/avatar';
import {UITextInput} from '@components/ui/form';
import {UISubmitButton} from '@components/ui/button';
import {UIIconFeather} from '@components/ui/icons';
import {resizeImage} from '@helpers/upload';
import firebase from 'react-native-firebase';

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        image: '',
        displayName: '',
      },
      loading: false,
    };
  }

  componentDidMount() {
    const {user} = this.props;
    const {form} = this.state;
    this.setState({
      form: {
        ...form,
        image: user.data.image,
        displayName: user.data.displayName,
      },
    });
  }

  async handleUpload() {
    const {form} = this.state;
    const options = {
      title: 'Select Avatar',
      mediaType: 'photo',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    await ImagePicker.launchImageLibrary(options, response => {
      if (response.error) {
        ToastAndroid.show(response.error, ToastAndroid.LONG);
      } else if (!response.didCancel) {
        this.setState({
          form: {
            ...form,
            image: response.uri,
          },
        });
      } else {
        return;
      }
    });
  }

  handleChange(name, value) {
    const {form} = this.state;
    this.setState({
      form: {
        ...form,
        [name]: value,
      },
    });
  }

  async handleSubmit() {
    const {user, navigation, editProfile} = this.props;
    const {loading} = this.state;
    const {form} = this.state;
    this.setState({loading: true});
    if(user.data.image !== form.image ){
    const data = await resizeImage(
      form.image,
      700,
      700,
      'JPEG',
      user.data.username,
    );
    firebase
      .storage()
      .ref(`${'user-image'}/${data.data.image}`)
      .putFile(data.data.uri)
      .on(
        firebase.storage.TaskEvent.STATE_CHANGED,
        snapshot => {
          if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
            editProfile(
              {image: snapshot.downloadURL, name: form.displayName},
              'refresh',
              navigation,
            );
            this.setState({loading: false});
          }
        },
        error => {
          unsubscribe();
          ToastAndroid.show(
            'Upload image gagal, coba sekali lagi!',
            ToastAndroid.LONG,
          );
          this.setState({loading: false});
          return;
        },
      );
    }
    else{
      editProfile(
        {name: form.displayName},
        'refresh',
        navigation,
      );
    }
  }

  render() {
    const {form, loading} = this.state;
    const {navigation, user} = this.props;
    let {strings} = this.props.screenProps;
    const disabled =
      form.image !== user.data.image ||
      form.displayName !== user.data.displayName;
    return (
      <SafeAreaView style={[styles.flex]}>
        <LayHeader
          navigation={navigation}
          title={strings('profile.text.change-profile')}
        />
        <Layout style={styles.containercenter}>
          <TouchableOpacity
            onPress={() => this.handleUpload()}
            activeOpacity={0.8}
            style={styles.mbot20}>
            <UIAvatar size={150} source={{uri: form.image}} />
            <View style={styles.stickyTopIcon}>
              <UIIconFeather
                name="edit-2"
                size={16}
                style={styles.stickychildIcon}
              />
            </View>
          </TouchableOpacity>
          <UITextInput
            label="Nama"
            value={form.displayName}
            onChangeText={value => this.handleChange('displayName', value)}
          />
          <UISubmitButton
            title={strings('profile.text.change-profile')}
            style={styles.mtop20}
            onPress={() => this.handleSubmit()}
            disabled={disabled}
            loading={loading ? true : false}
          />
        </Layout>
      </SafeAreaView>
    );
  }
}
export default Index;
