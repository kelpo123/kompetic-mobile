import React from 'react';
import {SafeAreaView} from 'react-native';
import RequireLogin from '@features/auth/container/requireLogin';
import {isEmpty} from 'lodash';
import styles from '@assets/styles';
import {Layout} from '@ui-kitten/components';
import {LayUserInfo} from '@components/layout/profile';

const index = props => {
  const {user, navigation} = props;
  let {strings} = props.screenProps;
  if (isEmpty(user.data) && !user.loading) {
    return <RequireLogin strings={strings} />;
  } else {
    return (
      <SafeAreaView style={[styles.flex]}>
        <Layout style={styles.section}>
          <LayUserInfo
            strings={strings}
            user={user}
            PressEdit={() => navigation.navigate('EditProfil')}
            PressSetting={() => navigation.navigate('Setting')}
          />
        </Layout>
      </SafeAreaView>
    );
  }
};

export default index;
