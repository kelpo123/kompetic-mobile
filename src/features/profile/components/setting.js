import React, {Component} from 'react';
import {SafeAreaView, View, Text} from 'react-native';
import styles from '@assets/styles';

import {LayHeader} from '@components/layout/header';
import {UIButtonText} from '@components/ui/button';
import {Layout} from '@ui-kitten/components';

class Index extends Component {
  handleLogout() {
    this.props.logout();
  }

  render() {
    const {navigation, logout} = this.props;
    let {strings} = this.props.screenProps;
    return (
      <SafeAreaView style={[styles.flex]}>
        <LayHeader
          navigation={navigation}
          title={strings('profile.setting.header')}
        />
        <Layout style={styles.container}>
          <UIButtonText
            name="edit-2"
            onPress={() => navigation.navigate("EditProfil")}
            title={strings('profile.text.change-profile')}
            withIcon
          />
           <UIButtonText
            name="corner-right-down"
            onPress={() => logout(navigation)}
            title={strings('auth.logout')}
            withIcon
          />
        </Layout>
      </SafeAreaView>
    );
  }
}
export default Index;
