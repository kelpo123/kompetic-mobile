import { connect } from "react-redux";
import {requestLogin} from "@redux/actions/auth"
import requireLogin from "../components/requireLogin";

function mapStateToProps(state){
  const {auth} = state.auth;
  return{
    auth
  }
}

function mapDispatchToProps(dispatch) {
  return {
    requestLogin: (fcm, navigation) => dispatch(requestLogin(fcm, navigation))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(requireLogin);
