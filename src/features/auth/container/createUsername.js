import { connect } from "react-redux";
import {requestEdit, resetHandle} from "@redux/actions/user"
import Index from "../components/createUsername";

function mapStateToProps(state){
  const {user} = state.user;
  return{
    user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    editProfile: (name, status, navigation) => dispatch(requestEdit(name, status, navigation)),
    resetHandle: () => dispatch(resetHandle())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Index);
