import {connect} from 'react-redux';
import {requestLogin, requestResendCode} from '@redux/actions/auth';
import Index from '../components/verifyPhone';

function mapStateToProps(state) {
  const {auth} = state.auth;
  return {
    auth,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    requestLogin: (fcm, navigation) =>
      dispatch(requestLogin(fcm, navigation)),
    resendCode: (number) => dispatch(requestResendCode(number))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Index);
