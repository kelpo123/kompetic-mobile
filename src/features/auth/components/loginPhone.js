import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native';
import {LayHeader} from '@components/layout/header';
import {UISubmitButton} from '@components/ui/button';
import {UITitle, UISubtitle} from '@components/ui/typography';
import {UITextInput} from '@components/ui/form';
import {Layout} from '@ui-kitten/components';
import {loginPhone} from '@helpers/firebase';
import styles from '@assets/styles';

const LoginPhone = props => {
  let {strings} = props.screenProps;
  const {navigation} = props;
  const [phoneNumber] = useState('');
  const [error, setError] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [loading, setLoading] = useState(false);

  const handleSubmit = async () => {
    setLoading(true);
    await loginPhone(phoneNumber)
      .then(confirmResult => {
        navigation.push('VerifyPhone', {
          number: '+62' + phoneNumber,
          confirmResult: confirmResult,
        });
      })
      .catch(() => {
        setError(true);
        setErrorMessage('Nomor tidak tersedia');
      });
    setLoading(false);
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <LayHeader navigation={navigation} title="Login" />
      <Layout style={styles.containercenter}>
        <UITitle title={strings('auth.phone.form.title')} center bot={10} />
        <UISubtitle
          title={strings('auth.phone.form.subtitle')}
          center
          cover
          bot={20}
        />
        <UITextInput
          phone
          autoFocus
          error={error}
          errorMessage={errorMessage}
          value={phoneNumber}
          onChangeText={value => this.setState({phoneNumber: value})}
          keyboardType={'phone-pad'}
          placeholder={strings('auth.phone.form.placeholder')}
        />
      </Layout>
      <UISubmitButton
        disabled={phoneNumber.length > 7 ? false : true}
        title={strings('form.next')}
        loading={loading}
        onPress={() => handleSubmit()}
      />
    </SafeAreaView>
  );
};

export default LoginPhone;
