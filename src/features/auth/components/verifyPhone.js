import React, {Component} from 'react';
import {SafeAreaView, ToastAndroid} from 'react-native';
import {Layout} from '@ui-kitten/components';
import styles from '@assets/styles';

import {LayHeader} from '@components/layout/header';
import {UISubmitButton} from '@components/ui/button';
import {UITitle, UISubtitle} from '@components/ui/typography';
import {UITextInputInfo} from '@components/ui/form';

//extra
import {getFcmToken, getTokenFirebase} from '@helpers/firebase';
import {storage} from '@helpers/storage';

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      codeVerify: '',
      countdown: false,
      fcmToken: '',
    };
  }

  componentDidMount() {
    this.FcmToken();
  }

  FcmToken() {
    getFcmToken()
      .then(fcmToken => {
        this.setState({fcmToken: fcmToken});
      })
      .catch(err => {
        ToastAndroid.show('Server sedang bermasalah', ToastAndroid.LONG);
      });
  }

  handleChange(value) {
    this.setState({
      codeVerify: value,
    });
    if (value.length == 6) {
      this.handleSubmit(value);
    }
  }

  async handleSubmit(value) {
    const {fcmToken} = this.state;
    const {navigation, requestLogin} = this.props;
    const {confirmResult} = this.props.navigation.state.params;
    this.setState({loading:true})
    await confirmResult.confirm(value)
      .then(() => {
        getTokenFirebase()
          .then(idToken => {
            storage.sendToken(idToken);
            requestLogin(fcmToken, navigation);
          })
          .catch(err => {
            ToastAndroid.show('Server sedang bermasalah', ToastAndroid.LONG);
          });
      })
      .catch(err => {
        ToastAndroid.show(
          'Kode Verifikasi Salah',
          ToastAndroid.LONG,
        );
      });
    this.setState({loading:false})
  }

  handleSendCode() {
    const {resendCode} = this.props;
    const {number} = this.props.navigation.state.params;
    this.setState({countdown: true});
    resendCode(number);
  }

  render() {
    let {strings} = this.props.screenProps;
    const {auth, navigation} = this.props;
    const {codeVerify, countdown, loading} = this.state;
    const {number} = this.props.navigation.state.params;
    return (
      <SafeAreaView style={{flex: 1}}>
        <LayHeader navigation={navigation} title="Login" />
        <Layout style={styles.containercenter}>
          <UITitle
            title={strings('auth.phone.form.verify-title')}
            center
            bot={10}
          />
          <UISubtitle
            title={strings('auth.phone.form.verify-subtitle', {name: number})}
            center
            cover
          />
          <Layout style={[styles.stretch, styles.mtop20]}>
            <UITextInputInfo
              strings={strings}
              onPress={() => this.handleSendCode()}
              onChangeText={(value) => this.handleChange(value)}
              countdown={countdown}
              value={codeVerify}
              keyboardType={'numeric'}
              countdownOut={() => this.setState({countdown: false})}
              error={auth.errorCode}
              disabled={loading ? true : false}
              errorMessage={auth.errorMessage}
              placeholder={strings('auth.phone.form.verify-placeholder')}
            />

          </Layout>
        </Layout>
        <UISubmitButton
          disabled={codeVerify.length > 5 ? false : true}
          loading={loading}
          title={strings('auth.phone.form.verify-finish')}
          onPress={() => this.handleSubmit(codeVerify)}
        />
      </SafeAreaView>
    );
  }
}
export default Index;
