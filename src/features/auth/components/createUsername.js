/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-hooks/rules-of-hooks */
import React, {useEffect, useState} from 'react';
import {SafeAreaView, BackHandler} from 'react-native';
import {UISubmitButton} from '@components/ui/button';
import {UITitle, UISubtitle} from '@components/ui/typography';
import {UITextInput} from '@components/ui/form';
import {Layout} from '@ui-kitten/components';
import styles from '@assets/styles';

const createUsername = props => {
  let {strings} = props.screenProps;
  const {navigation, user, editProfile, resetHandle} = this.props;
  const [name, setName] = useState('');

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      handleBackPress,
    );
    return () => {
      backHandler.remove();
    };
  }, []);

  const handleBackPress = () => {
    if (user.errorCode == 400) {
      resetHandle();
    }
    navigation.popToTop();
    return true;
  };

  const handleSubmit = () => {
    editProfile({username: name}, 'loading');
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <Layout style={styles.containercenter}>
        <UITitle
          title={strings('auth.phone.form.username-title')}
          center
          bot={10}
        />
        <UISubtitle
          title={strings('auth.phone.form.username-subtitle')}
          center
          cover
        />
        <UITextInput
          style={styles.mtop30}
          name="user"
          autoFocus
          error={user.errorCode}
          errorMessage={user.errorMessage}
          value={name}
          onChangeText={value => this.setState({name: value})}
          placeholder={strings('auth.phone.form.username-placeholder')}
        />
      </Layout>
      <UISubmitButton
        disabled={name.length > 3 ? true : false}
        title={strings('form.make')}
        loading={user.loading}
        onPress={() => handleSubmit()}
      />
    </SafeAreaView>
  );
};

export default createUsername;
