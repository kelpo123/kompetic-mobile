import React, {Component} from 'react';
import {LayRequireLogin} from '@components/layout/auth';
import {withNavigation} from 'react-navigation';
import {UISpinner} from '@components/ui/extra';
import {Layout} from '@ui-kitten/components';
import {ToastAndroid} from 'react-native';
import {
  LoginFacebook,
  LoginCredential,
  getFcmToken,
  getTokenFirebase,
} from '@helpers/firebase';
import {storage} from '@helpers/storage';

class requireLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      fcmToken: '',
    };
  }
  componentDidMount() {
    this.FcmToken();
  }

  async handleLoginFacebook() {
    const {fcmToken} = this.state;
    const {requestLogin, navigation} = this.props;
    const credential = await LoginFacebook();
    this.setState({loading: true});
    await LoginCredential(credential)
      .then(res => {
        getTokenFirebase()
          .then(idToken => {
            storage.sendToken(idToken);
            requestLogin(fcmToken, navigation);
          })
          .catch(err => {
            ToastAndroid.show('Server sedang bermasalah', ToastAndroid.LONG);
          });
      })
      .catch(err => {
        ToastAndroid.show(
          'Login dengan social media sedang bermasalah',
          ToastAndroid.LONG,
        );
      });
    this.setState({loading: false});
  }

  FcmToken() {
    getFcmToken()
      .then(fcmToken => {
        this.setState({fcmToken: fcmToken});
      })
      .catch(err => {
        ToastAndroid.show('Server sedang bermasalah', ToastAndroid.LONG);
      });
  }

  render() {
    const {loading} = this.state;
    let {strings, navigation} = this.props;
    return (
      <Layout style={{flex: 1}}>
        <UISpinner loading={loading} />
        <LayRequireLogin
          strings={strings}
          onPressFacebook={() => this.handleLoginFacebook()}
          onPressMobile={() => navigation.push('LoginPhone')}
        />
      </Layout>
    );
  }
}
export default withNavigation(requireLogin);
