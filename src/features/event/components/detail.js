import React, {Component} from 'react';
import {SafeAreaView, ScrollView} from 'react-native';
import {LayHeader} from '@components/layout/header';
import {LayDetailEvent} from '@components/layout/detail/event';
import {DetailEvent} from '@constants/dummy';
import {UISubmitButton} from '@components/ui/button';
import {isEmpty} from 'lodash';
import styles from '@assets/styles';
class Index extends Component {
  render() {
    const {user, navigation} = this.props;
    let {strings} = this.props.screenProps;
    return (
      <SafeAreaView style={{flex: 1}}>
        <LayHeader navigation={navigation} title={'Detail'} />
        <ScrollView
          contentContainerStyle={[
            styles.container,
            styles.pbot90,
            styles.ptop20,
          ]}>
          <LayDetailEvent data={DetailEvent} strings={strings} />
        </ScrollView>
        {!isEmpty(user.data) && (
          <UISubmitButton
            title={strings('others.join').toUpperCase() + ' IDR 50000'}
            onPress={() => alert('registered event')}
          />
        )}
      </SafeAreaView>
    );
  }
}
export default Index;
