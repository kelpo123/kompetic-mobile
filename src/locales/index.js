import i18n from 'i18n-js';

// Import all locales
import en from './en.json';
import id from './id.json';

i18n.translations = {
  en,
  id
};
i18n.locale = 'id';

export function strings(name, params = {}) {
  return i18n.t(name, params);
};

export default i18n;
