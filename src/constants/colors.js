export const color = {
  MAIN:'#F4A72F',
  BLACK:'#262626',
  SUBBLACK:'#7f7f7f',
  ALERT:'#FF5B5E',
  INFO:'#1E7CFF',
  SUCCESS:'#88ED61',
};