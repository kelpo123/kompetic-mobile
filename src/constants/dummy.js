
  export const Result =
    {
      id: 1,
      uri: 'https://i.ya-webdesign.com/images/avatar-png-1.png',
      name: 'Alvin Presley',
    };


export const EventList = [
  {
    id: 1,
    kategori_icon:'gamepad',
    icon: 'dota-2',
    name: 'Asian Pasific dota pro circuit Indonesia',
    pendaftaran: 'pendaftaran hingga 22 januari 2021',
    fee: 'FREE',
    price: 1200000,
    people: 10,
    until: 32,
  },
  {
    id: 2,
    kategori_icon:'gamepad',
    icon: 'dota-2',
    name: 'Asian Pasific dota pro circuit Indonesia',
    pendaftaran: 'pendaftaran hingga 22 januari 2021',
    fee: 'FREE',
    price: 1200000,
    people: 10,
    until: 32,
  }];


export const DesignList = [
  {
    id: 1,
    kategori_icon:'design',
    icon: 'logo',
    name: 'Designing Logo Kompetic Festival',
    pendaftaran: 'pendaftaran hingga 22 januari 2021',
    fee: 'FREE',
    price: 4200000,
    people: 5,
    until: 16,
  },
  {
    id: 2,
    kategori_icon:'design',
    icon: 'logo',
    name: 'Asian Pasific dota pro circuit Indonesia',
    pendaftaran: 'pendaftaran hingga 22 januari 2021',
    fee: 'FREE',
    price: 1200000,
    people: 10,
    until: 32,
  }];

  export const DetailEvent =
    {
      id: 1,
      kategori_icon:'design',
      icon: 'logo',
      name: 'Designing Logo Kompetic Festival',
      pendaftaran: 'pendaftaran hingga 22 januari 2021',
      fee: 'FREE',
      price: 4200000,
      people: 5,
      until: 16,
      desc: "Nail that first impression with a stunning logo, and seal the deal with an unforgettable business card. Start a Design Contest and our designers will create a custom logo with business cards you’ll love, guaranteed.",
      aturan: [
        "Tidak boleh mengambil hak cipta orang lain",
        "Menggunakan kuning sebagai warna primary",
        "Mengunnakan font arial sans serif"
      ],
      date: "20 - 30 January 2020",
      hadiah: [
        "Rp.5,000,000 + Trophy Winner + Plakat",
        "Rp.3,000,000 + Plakat",
        "Rp.1,000,000 + Plakat"
      ],
    };
