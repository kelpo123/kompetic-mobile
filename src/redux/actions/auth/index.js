import {types, URL_LOGIN, AUTH_PORT} from '@constants/auth';
import {types as typesUser} from '@constants/user';
import firebase from 'react-native-firebase';
import api from '@helpers/api';
import {storage} from '@helpers/storage';
import {ToastAndroid} from 'react-native';

import NavigationServices from '@helpers/navigationServices';

export function requestResendCode(number) {
  return dispatch => {
    firebase
      .auth()
      .signInWithPhoneNumber(number)
      .then(() => {
        dispatch({type: types.AUTH_MOBILE_RESET});
      })
      .catch(() => {
        ToastAndroid.show('gagal mengirim kode baru', ToastAndroid.LONG);
      });
  };
}

export function requestLogin(fcmToken) {
  return async dispatch => {
    dispatch({type: types.AUTH_MOBILE_LOADING});
    const data = {data: {attributes: {fcm_token: fcmToken}}};
    api
      .post(AUTH_PORT, URL_LOGIN, data)
      .then(response => {
        storage.removeToken();
        storage.sendToken(response.data.data.attributes.token);
        if (response.data.data.attributes.firstLogin) {
          dispatch({type: types.AUTH_MOBILE_RESET});
          NavigationServices.navigate('CreateUsername');
        } else {
          storage.sendUser(response.data.data.attributes);
          dispatch({type: types.AUTH_MOBILE_SUCCESS});
          dispatch({
            type: typesUser.FETCHING_USERS_SUCCESS,
            data: response.data.data.attributes,
          });
          NavigationServices.popToTop();
        }
      })
      .catch(error => {
        if (error.code != 400) {
          ToastAndroid.show(error.message, ToastAndroid.LONG);
        }
        dispatch({
          type: types.FETCHING_MAKE_USERNAME_FAILED,
          code: error.code,
          message: error.message,
        });
      });
  };
}

export function resetHandle() {
  return dispatch => {
    dispatch({type: types.AUTH_MOBILE_RESET});
  };
}

export function requestLogout() {
  return dispatch => {
    storage.removeAll();
    NavigationServices.goBack();
    dispatch({type: typesUser.FETCHING_USERS_RESET});
  };
}
