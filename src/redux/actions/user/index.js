import {storage} from '@helpers/storage';
import {AUTH_PORT} from '@constants/auth';
import {types, URL_UPDATE_PROFIL} from '@constants/user';
import api from '@helpers/api';
import {ToastAndroid} from 'react-native';
import NavigationServices from '@helpers/navigationServices';

export function requestGetUser() {
  return async dispatch => {
    dispatch({type: types.FETCHING_USERS_LOADING});
    const user = await storage.getUser();
    if (user) {
      dispatch({type: types.FETCHING_USERS_SUCCESS, data: user});
    } else {
      dispatch({type: types.FETCHING_USERS_RESET});
    }
  };
}
export function requestEdit(req, status) {
  return dispatch => {
    if (status == 'loading') {
      dispatch({type: types.FETCHING_USERS_LOADING});
    }
    const data = {data: {attributes: req}};
    api
      .put(AUTH_PORT, URL_UPDATE_PROFIL, data)
      .then(response => {
        console.log(response.data.data);
        storage.sendToken(response.data.data.attributes.token);
        storage.sendUser(response.data.data.attributes);
        dispatch({
          type: types.FETCHING_USERS_SUCCESS,
          data: response.data.data.attributes,
        });
        NavigationServices.popToTop();
      })
      .catch(error => {
        if (error.code != 400) {
          ToastAndroid.show(error.message, ToastAndroid.LONG);
        }
        dispatch({
          type: types.FETCHING_USERS_FAILED,
          code: error.code,
          message: error.message,
        });
      });
  };
}

export function resetHandle() {
  return dispatch => {
    dispatch({type: types.FETCHING_USERS_RESET});
  };
}
