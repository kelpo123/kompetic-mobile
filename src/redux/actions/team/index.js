import {types, TEAM_PORT, URL_GET_TEAM} from '@constants/team';
import api from '@helpers/api';

export function getTeam() {
  return dispatch => {
    dispatch({type: types.FETCHING_TEAM_LOADING});
    api
      .get(TEAM_PORT, URL_GET_TEAM)
      .then(response => {
        dispatch({
          type: types.FETCHING_TEAM_SUCCESS,
          data: response.data.data.attributes,
        });
      })
      .catch(error => {
        dispatch({
          type: types.FETCHING_TEAM_FAILED,
          code: error.code,
          message: error.message,
        });
      });
  };
}
