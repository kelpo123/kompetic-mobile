import { types } from '@constants/user';
const initialState = {
    user:{
      data: '',
      loading:false,
      errorCode: null,
      errorMessage: '',
    }
  };
  const user = (state = initialState, action) => {
    switch (action.type) {
      case types.FETCHING_USERS_LOADING:
        return {
          ...state,
          user:{
            loading:true
          }
        };
      case types.FETCHING_USERS_SUCCESS:
        return {
          ...state,
          user:{
            data: action.data,
            loading:false
          }
        };
      case types.FETCHING_USERS_FAILED:
        return {
          ...state,
          user:{
            errorCode:action.code,
            errorMessage:action.message
          }
        };
      case types.FETCHING_USERS_RESET:
        return {
          ...state,
          user:{
            data: '',
            loading: false,
            errorCode: null,
            errorMessage: '',
          }
        };
      default:
        return state;
    }
  };
  export default user;
