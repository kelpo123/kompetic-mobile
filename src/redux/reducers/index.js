import {combineReducers} from 'redux';
import user from './user';
import auth from './auth';
import team from './team';
export default combineReducers({
  auth,
  user,
  team
});
