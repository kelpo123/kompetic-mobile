import {types} from '@constants/auth';
const initialState = {
  auth: {
    data: '',
    loading: false,
    errorCode:null,
    errorMessage: '',
  },
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case types.AUTH_MOBILE_LOADING:
      return {
        ...state,
        auth: {
          loading: true,
        }
      };
    case types.AUTH_MOBILE_FAILURE:
      return {
        ...state,
        auth: {
        errorCode:action.code,
        errorMessage: action.message
        }
      };
    case types.AUTH_MOBILE_FAILED:
      return {
        ...state,
        auth: {
        loading:false
        }
      };
    case types.AUTH_MOBILE_SUCCESS:
      return {
        ...state,
        auth: {
          loading:false,
          errorCode:null,
          errorMessage: '',
        }
      };
    case types.AUTH_MOBILE_RESET:
    return {
        ...state,
        auth: {
          data: '',
          loading: false,
          errorCode:null,
          errorMessage: '',
        }
    };
    default:
      return state;
  }
};
export default auth;
