import {types} from '@constants/team';
const initialState = {
  team: {
    data: '',
    loading: false,
    errorCode: null,
    errorMessage: '',
  },
};

const team = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCHING_TEAM_LOADING:
      return {
        ...state,
        team: {
          loading: true,
        },
      };
    case types.FETCHING_TEAM_SUCCESS:
      return {
        ...state,
        team: {
          loading: false,
          errorCode: null,
          errorMessage: '',
        },
      };
    case types.FETCHING_TEAM_FAILED:
      return {
        ...state,
        team: {
          loading: false,
        },
      };
    case types.FETCHING_TEAM_RESET:
      return {
        ...state,
        team: {
          data: '',
          loading: false,
          errorCode: null,
          errorMessage: '',
        },
      };
    default:
      return state;
  }
};
export default team;
