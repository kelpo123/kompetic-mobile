'use strict';
import {StyleSheet} from "react-native";
import {width} from "@constants/dimensions.js";
module.exports = StyleSheet.create({
  wrapper:{
    flex:1,
    backgroundColor: '#f9f9f9',
  },
  flex:{
    flex:1
  },
  column: {
    flexDirection: 'column',
  },
  row: {
    flexDirection: 'row',
  },
  container:{
    paddingLeft: 20,
    paddingRight: 20,
  },
  section:{
    borderBottomWidth:1,
    paddingVertical:20,
    borderColor: '#edf0f4'
  },
  sectionsmall:{
    borderBottomWidth:1,
    paddingVertical:15,
    borderColor: '#edf0f4'
  },
  centered:{
    alignSelf:'stretch',
    alignItems: 'center',
    justifyContent:'center',
  },
  containercenter:{
    flex:1,
    alignItems: 'center',
    justifyContent:'center',
    paddingLeft: 15,
    paddingRight: 15,
  },
  //align
  stretch:{
    alignSelf:'stretch'
  },
  justifyleft:{
    justifyContent: 'flex-start'
  },
  justifycenter:{
    justifyContent: 'center'
  },
  justifyright:{
    justifyContent: 'flex-end'
  },
  aligncenter:{
    alignItems: 'center',
  },
  alignright:{
    flex:1,
    alignItems: 'flex-end',
  },
  left:{
    alignItems: 'flex-start',
  },
  right:{
    alignItems: 'flex-end',
  },
  textcenter: {
    textAlign:'center',
  },
  textleft: {
    textAlign:'left',
  },
  textright: {
    textAlign:'right',
  },
  textOverlayRight:{
    position:'absolute',
    right:10,
    top:7,
    paddingHorizontal:10,
    paddingVertical:5,
    borderRadius:5
  },
  //space
  mtop5:    { marginTop:5 },
  mtop10:   { marginTop:10 },
  mtop15:   { marginTop:15 },
  mtop20:   { marginTop:20 },
  mtop25:   { marginTop:25 },
  mtop30:   { marginTop:30 },
  mtop50:   { marginTop:50 },
  mbot5:    { marginBottom:5 },
  mbot10:   { marginBottom:10 },
  mbot15:   { marginBottom:15 },
  mbot20:   { marginBottom:20 },
  mbot25:   { marginBottom:25 },
  mbot30:   { marginBottom:30 },
  mright1:  { marginRight:1 },
  mright5:  { marginRight:5 },
  mright10: { marginRight:10 },
  mright15: { marginRight:15 },
  mright20: { marginRight:20 },
  mleft5:   { marginLeft:5 },
  mleft10:  { marginLeft:10 },
  mleft15:  { marginLeft:15 },
  mleft20:  { marginLeft:20 },

  ptop5:    { paddingTop:5 },
  ptop10:   { paddingTop:10 },
  ptop15:   { paddingTop:15 },
  ptop20:   { paddingTop:20 },
  ptop25:   { paddingTop:25 },
  ptop30:   { paddingTop:30 },
  pbot5:    { paddingBottom:5 },
  pbot10:   { paddingBottom:10 },
  pbot15:   { paddingBottom:15 },
  pbot20:   { paddingBottom:20 },
  pbot25:   { paddingBottom:25 },
  pbot30:   { paddingBottom:30 },
  pbot90:   { paddingBottom:90 },
  pright5:  { paddingRight:5 },
  pright10: { paddingRight:10 },
  pright15: { paddingRight:15 },
  pright20: { paddingRight:20 },
  pleft5:   { paddingLeft:5 },
  pleft10:  { paddingLeft:10 },
  pleft15:  { paddingLeft:15 },
  pleft20:  { paddingLeft:20 },

  mh5:      { marginHorizontal:5 },
  mh10:     { marginHorizontal:10 },

  ph5:      { paddingHorizontal:5 },
  ph10:     { paddingHorizontal:10 },
  ph15:     { paddingHorizontal:15 },
  ph20:     { paddingHorizontal:20 },
  ph25:     { paddingHorizontal:25 },
  ph30:     { paddingHorizontal:30 },
  pv5:      { paddingVertical:5 },
  pv8:      { paddingVertical:8 },
  pv10:     { paddingVertical:10 },
  pv11:     { paddingVertical:11 },
  pv15:     { paddingVertical:15 },
  pv20:     { paddingVertical:20 },
  pv25:     { paddingVertical:25 },
  pv30:     { paddingVertical:30 },

  lh15:     { lineHeight: 15 },
  lh20:     { lineHeight: 20 },
  lh21:     { lineHeight: 21 },
  lh22:     { lineHeight: 22 },
  lh23:     { lineHeight: 23 },
  lh24:     { lineHeight: 24 },
  lh25:     { lineHeight: 25 },
  lh26:     { lineHeight: 26 },
  lh27:     { lineHeight: 27 },
  lh28:     { lineHeight: 28 },
  lh29:     { lineHeight: 29 },
  lh30:     { lineHeight: 30 },
  lh35:     { lineHeight: 35 },

  //typography
  title:{
    fontSize:22,
    fontFamily: 'AvenirNextLTPro-Bold',
    lineHeight:26
  },
  title:{
    fontSize:20,
    fontFamily: 'AvenirNextLTPro-Bold',
    lineHeight:24
  },
  titlesmall:{
    fontSize:16,
    fontFamily: 'AvenirNextLTPro-Bold',
  },
  titlesmaller:{
    fontSize:14,
    fontFamily: 'AvenirNextLTPro-Bold',
    lineHeight:22
  },
  titleheader:{
    fontSize:20,
    fontFamily: 'AvenirNextLTPro-Bold',
  },
  subtitle:{
    fontSize:16,
    fontFamily: 'AvenirNextLTPro-Medium',
  },
  subtitlesmall:{
    fontSize:14,
    fontFamily: 'AvenirNextLTPro-Medium',
  },
  subtitlesmaller:{
    fontSize:13,
    fontFamily: 'AvenirNextLTPro-Medium',
  },
  subtitleMedium:{
    fontSize:16,
    fontFamily: 'AvenirNextLTPro-Medium',
  },
  textButtonInfo:{
    fontSize:14,
    fontFamily: 'AvenirNextLTPro-Demi',
  },
  textRegular:{
    fontFamily: 'AvenirNextLTPro-Regular'
  },
  textButton:{
    fontSize:16,
    fontFamily: 'AvenirNextLTPro-Bold',
  },

  //button
  btn: {
    paddingHorizontal:20,
    paddingVertical:10,
    alignItems: 'center',
    borderRadius:5,
    width:width-120,
  },
  btnbottom:{
    position:'absolute',
    bottom:0,
    width:width,
    paddingVertical:20
  },
  buttonright:{
    justifyContent:'flex-end',
    alignItems:'flex-end',
    paddingHorizontal:20
  },
  stickyTopIcon:{
    backgroundColor:'#F6E62B',
    borderRadius:100,
    width:35,
    height:35,
    alignItems:'center',
    justifyContent:'center',
    position:'absolute',
    right:10
  },
  active:{
    opacity:1
  },
  disabled:{
    opacity:0.7,
    backgroundColor: '#dfe1e4'
  },

  imagemedium:{
    width:30,
    height:30
  },
  imagesmall:{
    width:15,
    height:15
  },
  iconInput:{
    position:'absolute',
    top:12,
    left:10,
    flexDirection:'row',
  },
  textInput:{
    backgroundColor: "#f5f5f5"
  },

  shadow:{
    elevation:3
  },
  cover:{
    width:width-80
  },

  //card
  cardhorizontal:{
    elevation:5,
    backgroundColor:'#ffffff',
    paddingVertical:15,
    paddingHorizontal:20,
    borderRadius:10,
    marginVertical:10,
    width: width-80
  }



});
