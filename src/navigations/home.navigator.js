import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import HomeComponent from '@features/home/containers';
import LoginComponent from '@features/auth/containers/loginPhone';
import VerifyPhone from '@features/auth/containers/verifyPhone';
import CreateUsername from '@features/auth/containers/createUsername';
import Team from '@features/team/containers';
import MakeTeam from '@features/team/containers/makeTeam';
import DetailEvent from '@features/event/containers/detail';

const Stack = createStackNavigator();

export default function MyStack() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Home" component={HomeComponent} />
      <Stack.Screen name="Login" component={LoginComponent} />
      <Stack.Screen name="VerifyPhone" component={VerifyPhone} />
      <Stack.Screen name="CreateUsername" component={CreateUsername} />
      <Stack.Screen name="Team" component={Team} />
      <Stack.Screen name="MakeTeam" component={MakeTeam} />
      <Stack.Screen name="DetailEvent" component={DetailEvent} />
    </Stack.Navigator>
  );
}
