// import 'react-native-gesture-handler';
import React from 'react';
import {DefaultTheme, NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {useTheme} from '@ui-kitten/components';
console.disableYellowBox = true;
/*
 * Navigation theming: https://reactnavigation.org/docs/en/next/themes.html
 */
import HomeStack from './home.navigator';
import NotifComponent from '@features/notification/containers';
import ProfilComponent from '@features/profile/containers';

import TabBarIcon from './components/tabBarIcon';

const navigatorTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    // prevent layout blinking when performing navigation
    background: 'transparent',
  },
};

const tabBarOptions = {
  labelStyle: {
    fontSize: 12,
    paddingTop: 0,
    fontFamily: 'Gotham-Medium',
  },
  style: {
    paddingBottom: 10,
    paddingTop: 10,
    height: 65,
    borderTopWidth: 0,
    elevation: 15,
    paddingHorizontal: 15,
  },
};

const Tab = createBottomTabNavigator();
export const AppNavigator = () => {
  const theme = useTheme();
  return (
    <NavigationContainer theme={navigatorTheme}>
      <Tab.Navigator
        tabBarOptions={{
          ...tabBarOptions,
          activeTintColor: theme['color-primary-500'],
          inactiveTintColor: theme['color-primary-300'],
        }}>
        <Tab.Screen
          name="Home"
          component={HomeStack}
          options={{
            tabBarLabel: 'Explore',
            tabBarIcon: ({color}) => (
              <TabBarIcon name="home" color={color} size={22} />
            ),
          }}
        />
        <Tab.Screen
          name="Cart"
          component={NotifComponent}
          options={{
            tabBarLabel: 'Cart',
            tabBarIcon: ({color}) => (
              <TabBarIcon name="shopping-cart" color={color} size={27} />
            ),
          }}
        />
        <Tab.Screen
          name="Settings"
          component={ProfilComponent}
          options={{
            tabBarLabel: 'Me',
            tabBarIcon: ({color}) => (
              <TabBarIcon name="person" color={color} size={27} />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};
