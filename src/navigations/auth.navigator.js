import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SignInScreen from '@screen/signin';
import {HomeScreen} from '@screen/home/containers';
const Stack = createStackNavigator();

export const AuthNavigator = props => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen
        name="SignIn"
        component={SignInScreen}
        header="none"
        options={{
          headerShown: false,
          // When logging out, a pop animation feels intuitive
          // You can remove this if you want the default 'push' animation
          // animationTypeForReplace: state.isSignout ? 'pop' : 'push',
        }}
      />
    </Stack.Navigator>
  );
};

AuthNavigator.propTypes = {};
