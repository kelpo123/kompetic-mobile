//Home Route
import {TabNavigator} from './pages/bottomNavigation.js';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import {NavigationConfig} from './components/slideAnimation'
import LoginPhone from '@features/auth/container/loginPhone';
import VerifyPhone from '@features/auth/container/verifyPhone';
import CreateUsername from '@features/auth/container/createUsername';
import Team from '@features/team/container';
import MakeTeam from '@features/team/container/makeTeam'
import DetailEvent from '@features/event/container/detail'

const HomeStack = createStackNavigator({
    Home: { screen: TabNavigator },
    LoginPhone: {screen: LoginPhone},
    VerifyPhone: {screen: VerifyPhone},
    CreateUsername: {screen: CreateUsername},
    Team: {screen: Team},
    MakeTeam: {screen: MakeTeam},
    DetailEvent: {screen: DetailEvent}
  }, {
    headerMode: 'none',
    transitionConfig: NavigationConfig,
   navigationOptions:{
      headerVisible: false
   }
  });

export default createAppContainer(HomeStack);