import * as React from 'react';
import styles from '@assets/styles';
import {Icon} from '@ui-kitten/components';

export default function TabBarIcon(props) {
  return (
    <Icon
      style={styles.extraSmall}
      fill={color}
      width={props.size}
      name={props.name}
    />
  );
}
