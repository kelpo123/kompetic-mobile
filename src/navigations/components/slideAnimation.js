
// Transition
export const Transition = (index, position, width) => {
	const sceneRange = [index-1, index, index+1];
	const outputWidth = [width, 0, 0];
	const transition = position.interpolate({
		inputRange: sceneRange,
		outputRange: outputWidth,
	});

	return {
		transform:[{ translateX:transition}]
	}
}

export const NavigationConfig = () => {
	return {
		screenInterpolator: (sceneProps) => {
			const position = sceneProps.position;
			const scene = sceneProps.scene;
			const index = scene.index;
			const width = sceneProps.layout.initWidth;
			return Transition(index, position, width);
		}
	}
}
// End Transition
